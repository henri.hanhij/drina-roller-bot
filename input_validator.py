import discord
import random
import pathlib
import settings
from discord.ext import commands #.VENV BROKEN LMAO????

def validate_args(args_x, args_y, args_z, ctx_type='roll'):
    mn = 5
    if ctx_type == 'd6':
        mn = 2
    try:
        return int(args_x), int(args_y), min(int(args_z), mn)
    except ValueError:
        raise Exception('invalid input')