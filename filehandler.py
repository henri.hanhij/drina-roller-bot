import discord
import random
import pathlib
import settings
from discord.ext import commands #.VENV BROKEN LMAO????

async def roll_file(ctx, arg_a):

    if(arg_a == "d6" or arg_a == "flip"):
        file = random.choice(list(pathlib.Path(settings.BASE_DIR / "gifdata" / arg_a).glob("**/*")))
        file_image_file = discord.File(
            file, filename=file.name
        )
        #embed = discord.Embed(title=file.name[10:15])
        embed = discord.Embed(title=None)
        embed.set_image(url=f"attachment://{file.name}")

        await ctx.send(embed=embed, file=file_image_file)