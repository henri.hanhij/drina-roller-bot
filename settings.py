import os
import pathlib
import discord
from dotenv import load_dotenv

load_dotenv()

TOKEN = os.getenv("D_TOKEN")

BASE_DIR = pathlib.Path(__file__).parent