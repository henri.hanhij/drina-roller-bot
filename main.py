import settings
import discord
import random
import pathlib
import filehandler
import input_validator
from discord.ext import commands #.VENV BROKEN LMAO????

def run():

    #DECLARE PERM GRID
    intents = discord.Intents.default()
    intents.message_content = True
    bot = commands.Bot(command_prefix="!", intents=intents)

    #ACTIVATION
    @bot.event
    async def on_ready():
        print(bot.user)
        print(bot.user.id)

    #LISTEN CONTEXT
    #IGNORE TREE.COMMAND??
    @bot.command()
    async def roll(ctx, args_x='def', args_y='def', args_z=1):
        """roll X-Y for Z times (def=1, max=5)"""
        if args_x =='def' and args_y == 'def':
            args_x, args_y = 0, 100000
        elif args_x !='def' and args_y == 'def':
            args_y, args_x = args_x, 0

        args_x, args_y, args_z = input_validator.validate_args(args_x, args_y, args_z)

        if args_y < args_x:
            await ctx.send("error")
        else:
            for r in range(args_z):
                await ctx.send(f"!roll {args_x}-{args_y}: **{random.randint(args_x, args_y)}**")

    @bot.command()
    async def multiroll(ctx, args_z=5):
        """roll 0-100000 for Z times (def=5, max=5)"""
        args_x, args_y, args_z = input_validator.validate_args(0, 100000, args_z)

        for r in range(args_z):
            await ctx.send(f"!roll 0-100000: **{random.randint(0, 100000)}**")
    
    @bot.command()
    async def d6(ctx, args_z=1):
        """roll d6"""
        args_x, args_y, args_z = input_validator.validate_args(0, 0, args_z, 'd6')

        for r in range(args_z):
            await filehandler.roll_file(ctx, "d6")

    @bot.command()
    async def flip(ctx):
        """flip coin"""
        await filehandler.roll_file(ctx, "flip")

    bot.run(settings.TOKEN)
        
if __name__ == "__main__":
    run()